# A palindromic number reads the same both ways. The largest palindrome made
# from the product of two 2-digit numbers is 9009 = 91 x 99

# Find the largest palindrome made from the product of two 3-digit numbers

def isPalindrome(n: int) -> bool:
    return (str(n) == str(n)[::-1])


def main():
    biggestPalindrome = 0
    for i in range(100, 1000):
        for j in range(100, 1000):
            if (isPalindrome(i*j) and (biggestPalindrome < (i*j))):
                biggestPalindrome = i*j
                multipliers = (i, j)
    print(biggestPalindrome, multipliers)


main()
