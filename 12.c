#include <math.h>
#include <stdio.h>

long count_divisors(long n) {
  long counter = 0;
  long end = sqrt(n);
  for (long i = 1; i < end; i++) {
    if (n % i == 0) {
      counter += 2;
    }
  }
  if (end * end == n) {
    counter--;
  }
  return counter; // to include the number itself
}

int main(void) {
  long triangle_number = 0;
  for (long i = 1;; i++) {
    triangle_number += i;
    if (count_divisors(triangle_number) > 500) {
      break;
    }
  }
  printf("%li \n", triangle_number);
}
