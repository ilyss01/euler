/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17

Find the sum of all the primes below two million
*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#define BIG_NUBMER 2000000

bool isPrime(long n) {
  for (long i = 3; i <= (long)(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main(void) {
  long sum = 0;
  sum += 2;
  for (long i = 3; i <= BIG_NUBMER; i += 2) {
    if (isPrime(i)) {
      sum += i;
    }
  }

  printf("%li\n", sum);
}
