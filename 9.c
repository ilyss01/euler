/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2

There exists exactly one Pythagorean triplet for which a + b + c = LIMIT
Find the product abc
*/
#include <stdio.h>
#define LIMIT 1000

int main(void) {
  int a, b, c;

  for (a = 1; a < LIMIT; a++) {
    for (b = a + 1; b < LIMIT; b++) {
      c = LIMIT - a - b;
      if (a * a + b * b == c * c) {
        printf("%i %i %i\n", a, b, c);
        return 0;
      }
    }
  }
}
