/*
The sum of the squares of the first ten natural numbers is, 1^2 + 2^2 + ... +
10^2 = 385

The square of the sum of the first ten natural numbers is, (1 + 2 + ... + 10)^2
= 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural
numbers and the square of the sum is 3025 - 385 = 2640

Find the difference between the sum of the squares of the first one hundred
natural numbers and the square of the sum
*/

#include <stdio.h>
#define LIMIT 100

int main(void) {
  int answer = 0;

  long sum_of_squares = 0;
  for (int i = 1; i <= LIMIT; i++) {
    sum_of_squares += i * i;
  }

  long square_of_sum = (LIMIT / 2) * (LIMIT + 1);
  square_of_sum *= square_of_sum;

  answer = square_of_sum - sum_of_squares;

  printf("%i\n", answer);
}
