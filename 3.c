/*
The prime factors of 13195 are 5, 7, 13 and 29

What is the largest prime factor of the number 600851475143
*/
#include <math.h>
#include <stdbool.h>
#include <stdio.h>

bool isPrime(long n) {
  if (n % 2 == 0) {
    return false;
  }
  for (long i = 3; i <= (long)(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main(void) {
  long bigNumber = 600851475143;
  long limit = (long)(sqrt(bigNumber));
  long biggestPrime;
  for (long i = 3; i < (limit + 1); i += 2) {
    if (isPrime(i)) {
      if (bigNumber % i == 0) {
        bigNumber /= i;
        biggestPrime = i;
      }
    }
  }
  printf("%li\n", biggestPrime);
}
