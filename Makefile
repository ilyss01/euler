CC = gcc
CFLAGS = -std=c17 -Wextra -Wall 
LDFLAGS = -lm

FORMATC = clang-format
FORMATCFLAGS = --style=LLVM -i

FORMATPY = autopep8
FORMATPYFLAGS = --in-place

make:
	$(CC) $(CFLAGS) $(LDFLAGS) *.c

format:
	$(FORMATC) $(FORMATCFLAGS) *.c

autopep:
	$(FORMATPY) $(FORMATPYFLAGS) *.py
