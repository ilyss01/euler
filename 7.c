/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13

What is the 10001st prime number?
*/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#define TARGET 10001

bool isPrime(int n) {
  if (n % 2 == 0) {
    return false;
  }
  for (int i = 3; i <= (int)(sqrt(n)); i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int main(void) {
  int counter = 1;
  int target = 2;
  for (int i = 3; counter != TARGET; i += 2) {
    if (isPrime(i)) {
      counter++;
      target = i;
    }
  }
  printf("%i\n", target);
}
