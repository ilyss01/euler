/*
2520 is the smallest number that can be divided by each of the numbers from 1
to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?
*/

/*
The smallest number n that is evenly divisible by every number in a set {k1,
k2,
..., k_m} is also known as the lowest common multiple (LCM) of the set of
numbers. The LCM of two natural numbers x and y is given by LCM(x, y) = x * y /
GCD(x, y). When LCM is applied to a collection of numbers, it is commutative,
associative, and idempotent. Hence LCM(k1, k2, ..., k_m) = LCM(...(LCM(LCM(k1,
k2), k3)...), k_m).
*/
#include <stdio.h>
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define LIMIT 21

int GCD(int x, int y) {
  for (int i = max(x, y); i > 0; i -= min(x, y)) {
    if ((x % i == 0) && (y % i == 0)) {
      return i;
    }
  }
  return 1;
}

int LCM(int x, int y) { return (x * (y / GCD(x, y))); }

int main(void) {
  long answer = 1;
  for (int i = 1; i < LIMIT; i++) {
    answer = LCM(answer, i);
  }

  printf("%li\n", answer);
}
